package main

import (
	"crypto/sha256"
	"encoding/xml"
	"flag"
	"fmt"
	"os"
	"strings"
)

type Entry struct {
	Text     string `xml:"seg"`
	Language string `xml:"lang,attr"`
}

type Sentence struct {
	Text []Entry `xml:"tuv"`
}

var (
	inputFile = os.Getenv("TMX_INFILE")
	outFile   = os.Getenv("TMX_OUTFILE")
	indexName = os.Getenv("TMX_INDEX")
	out       = os.Stdout
)

func init() {

	if inputFile == "" {
		inputFile = "cs-de_small.tmx"
	}

	if indexName == "" {
		indexName = "test"
	}

	flag.StringVar(&inputFile, "i", inputFile, "Input file path.")
	flag.StringVar(&indexName, "index", indexName, "Name of the index for the bulk file.")
	flag.StringVar(&outFile, "o", outFile, "Raw output file.")

	flag.Parse()

	if outFile == "" {
		out = os.Stdout
	} else {
		var err error
		out, err = os.Create(outFile)
		if err != nil {
			panic(err)
		}
	}
}

func Parse(inputFile string) {

	xmlFile, err := os.Open(inputFile)
	if err != nil {
		fmt.Println("Error opening file: ", err)
		return
	}
	defer xmlFile.Close()

	decoder := xml.NewDecoder(xmlFile)

	for {
		// Read the tokens from XML in a stream
		t, _ := decoder.Token()
		if t == nil {
			break
		}

		// Inspect the type of the tokens
		switch se := t.(type) {
		case xml.StartElement:
			// If this is a tu token
			if se.Name.Local == "tu" {
				var s Sentence
				// Decode a whole chunk of XML into variable
				decoder.DecodeElement(&s, &se)

				t1 := strings.Replace(strings.Trim(s.Text[1].Text, " -()\"'"), "\"", "\\\"", -1)
				uuid := sha256.Sum256([]byte(t1))

				fmt.Fprintf(out, "{\"index\":{\"_index\":\"%s\", \"_type\":\"sentence\"}}\n", indexName)
				fmt.Fprintf(out, "{ \"uuid\": \"%x\", \"language1\":\"%s\",\"sentence1\":\"%s\",\"language2\":\"%s\",\"sentence2\":\"%s\" }\n",
					uuid,
					s.Text[1].Language,
					t1,
					s.Text[0].Language,
					strings.Replace(strings.Trim(s.Text[0].Text, " -()\"'"), "\"", "\\\"", -1))
			}
		default:
		}
	}
	fmt.Println()
}

func main() {
	Parse(inputFile)
}
